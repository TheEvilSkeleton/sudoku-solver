mod imp;

use adw::subclass::prelude::*;
use gtk::{glib, traits::{ButtonExt, WidgetExt}, prelude::{ObjectExt, ToVariant}};
use glib::{clone, Object};

use super::sudoku_grid::SudokuGrid;
use crate::backend::sudoku::Sudoku;

use serde::{Deserialize, Serialize};

glib::wrapper! {
    pub struct SudokuRow(ObjectSubclass<imp::SudokuRow>)
        @extends adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow, gtk::Widget,
        @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl SudokuRow {
    pub fn new(title: &str) -> Self {
        Object::builder()
            .property("title", title)
            .build()
    }

    pub fn setup_actions(&self){
        self.imp()
        .btn_delete
        .connect_clicked(clone!(@weak self as widget => move |_| {
            let name = widget.property::<String>("title");
            widget
            .activate_action("win.delete-sudoku", Some(&name.to_variant()))
            .expect("The action does not exist.");
        }));
    }

    pub fn set_grid(&self, grid: SudokuGrid) {
        self.imp().sudoku_grid.set(grid)
        .expect("Could not set `SudokuGrid`");
    }

    pub fn to_row_data(&self) -> SudokuRowData {
        let title = self.property::<String>("title");
        let sudoku = self.imp()
                        .sudoku_grid
                        .get()
                        .expect("Could not get `SudokuGrid`")
                        .sudoku()
                        .clone();


        SudokuRowData { title, sudoku }
    }

    pub fn from_row_data(data: SudokuRowData) -> Self {
        let title = data.title;
        let sudoku = data.sudoku;
        let sudoku_grid = SudokuGrid::new();
        sudoku_grid.sudoku().set_sudoku(sudoku);
        sudoku_grid.set_cell_sensitivity();

        let row = Self::new(&title);
        row.set_grid(sudoku_grid);

        return row;
    }
}

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct SudokuRowData {
    pub title: String,
    pub sudoku: Sudoku,
}
