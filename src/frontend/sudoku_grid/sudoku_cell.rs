use adw::subclass::prelude::*;
use gtk::glib;
use glib::Object;

mod imp {
    use adw::prelude::*;
    use adw::subclass::prelude::*;
    use gtk::{glib, MenuButton, CompositeTemplate};
    use glib::subclass::InitializingObject;

    #[derive(Default, CompositeTemplate)]
    #[template(resource = "/io/gitlab/cyberphantom52/sudoku_solver/ui/sudoku_cell.ui")]
    pub struct SudokuCell {
        #[template_child]
        pub cell: TemplateChild<MenuButton>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SudokuCell {
        const NAME: &'static str = "SudokuCell";
        type Type = super::SudokuCell;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl SudokuCell {
        #[template_callback]
        fn number_pressed(&self, button: &gtk::Button) {
            let num = button.label()
                .expect("The widget should have a label")
                .parse::<u8>()
                .expect("The value should be of the type `u8`");

            self.cell.set_label(&num.to_string());

            self.cell.popover()
                .expect("The widget should have a popover")
                .downcast::<gtk::Popover>()
                .expect("The widget should be of the type `gtk::Popover`")
                .popdown();
        }

        #[template_callback]
        fn reset(&self, _button: &gtk::Button) {
            self.cell.set_label(" ");

            self.cell.popover()
                .expect("The widget should have a popover")
                .downcast::<gtk::Popover>()
                .expect("The widget should be of the type `gtk::Popover`")
                .popdown();
        }
    }

    impl ObjectImpl for SudokuCell {}
    
    impl BinImpl for SudokuCell {}

    impl WidgetImpl for SudokuCell {}
}

glib::wrapper! {
    pub struct SudokuCell(ObjectSubclass<imp::SudokuCell>)
        @extends adw::Bin, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl SudokuCell {
    pub fn new() -> Self {
        Object::builder()
            .build()
    }

    pub fn get(&self) -> &gtk::MenuButton {
        self.imp()
        .cell
        .as_ref()
    }
}

