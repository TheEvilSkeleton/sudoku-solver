pub mod sudoku_grid;
pub mod sudoku_cell;

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;
use glib::{clone, Object, GString};

use sudoku_cell::SudokuCell;
use crate::backend::sudoku::Sudoku;

glib::wrapper! {
    pub struct SudokuGrid(ObjectSubclass<sudoku_grid::SudokuGrid>)
        @extends adw::Bin, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl SudokuGrid {
    pub fn new() -> Self {
        Object::builder()
            .build()
    }

    pub fn set_cell_sensitivity(&self) {
        let sudoku = self.sudoku();
        for row in 0..9 {
            for column in 0..9 {
                let cell = self.get_cell(row, column);
                if sudoku.get(row, column).is_fixed() {
                    cell.set_sensitive(false);
                }
            }
        }
    }

    pub fn set_fixed(&self) {
        for row in 0..9 {
            for column in 0..9 {
                let cell = self.get_cell(row, column).get();
                let value = cell
                    .label()
                    .expect("Cell should have a label");

                if value != " " {
                    self.imp()
                        .sudoku
                        .get()
                        .expect("`sudoku` must be set in `setup_sudoku()` before calling this method.")
                        .get(row, column)
                        .set_fixed();
                    
                    cell.set_sensitive(false);
                }
            }
        }
    }

    fn get_cell(&self, row: usize, column: usize) -> &SudokuCell {
        &self.imp()
            .sudoku_cells
            .get()
            .expect("`sudoku_cells` must be set in `setup_sudoku()` before calling this method.")
            [row][column]
    }

    fn update_cell(&self, sudoku: &Sudoku, row: usize, column: usize) {
        let cell = self.get_cell(row, column).get();
        
        let mut error_count = self.property::<u32>("num-error");
        if cell.css_classes().contains(&GString::from("error"))
            && sudoku.is_valid(row, column) {
                cell.remove_css_class("error");
                error_count -= 1;
        } else if !sudoku.is_valid(row, column) 
            && !cell.css_classes().contains(&GString::from("error")) {
                cell.add_css_class("error");
                error_count += 1;
        }
        
        self.set_property("num-error", error_count);
        self.emit_by_name("num-error-changed", &[&error_count])
    }

    fn build_ui(&self) {
        /*
        This function maps the 9x9 cells vector to the grid in a special way
        A 3x3 section of the overall vector is taken and mapped to buttons in a 3x3 GtkGrid
        In this way we are able to map each button by its coordinate to it's appropriate counterpart
        in the actual sudoko backend.
        */

        // The minimum size of the grid necessary to fit all the cells without any overflows
        self.set_height_request(485);

        
        let frame = gtk::AspectFrame::builder()
            .obey_child(false)
            .ratio(1.0)
            .build();

        let frame_grid = gtk::Grid::builder()
            .row_spacing(5)
            .column_spacing(5)
            .css_classes(vec!["grid".to_string()])
            .build();

        for outer_row in 0..3 {
            for outer_col in 0..3 {
                let box_grid = gtk::Grid::builder()
                    .row_spacing(2)
                    .column_spacing(2)
                    .hexpand(true)
                    .vexpand(true)
                    .build();

                for inner_row in 0..3 {
                    for inner_col in 0..3 {
                        let (row, column) = (3*outer_row + inner_row, outer_col*3 + inner_col);

                        box_grid.attach(
                            self.get_cell(row, column),
                            column as i32,
                            row as i32,
                            1,
                            1
                        );
                    }
                }

                frame_grid.attach(
                    &box_grid,
                    outer_col as i32,
                    outer_row as i32,
                    1,
                    1
                );
            }
        }
        
        frame.set_child(Some(&frame_grid));
        self.set_child(Some(&frame));
    }

    fn setup_sudoku(&self) {
        self.imp()
            .sudoku
            .set(Sudoku::new())
            .expect("Failed to set `Sudoku`");

        let mut cells = Vec::<Vec<SudokuCell>>::new();
        
        self.set_property("num-error", 0u32);
        
        for i in 0..9 {
            let mut row = Vec::<SudokuCell>::new();   
            for j in 0..9 {
                let cell = SudokuCell::new();
                cell.get().connect_label_notify(
                    clone!(@weak self as grid => move |cell| {
                        let sudoku = grid.sudoku();

                        let value = cell.label()
                            .expect("Cell should have a label")
                            .parse::<u8>()
                            .unwrap_or(0);

                        sudoku.get(i, j).set_val(value);

                        grid.update_grid(&sudoku, i, j);
                    })
                );
                row.push(cell);
            }
            cells.push(row);
        }
        self.imp().sudoku_cells.set(cells).expect("Could not set `sudoku_cells`");
    }

    fn update_grid(&self, sudoku : &Sudoku, row : usize, column: usize) {
        for i in 0..9 {
            // Row
            self.update_cell(sudoku, row, i);
            
            // Column
            self.update_cell(sudoku, i, column);

            // Box
            self.update_cell(
                sudoku, 
                row - row % 3 + i / 3, 
                column - column % 3 + i % 3
            );
        }
    }

    pub fn sudoku(&self) -> &Sudoku {
        self.imp().sudoku
            .get()
            .expect("`Sudoku` must be set first")
    }

    pub fn refresh(&self) {
        let sudoku = self.imp().sudoku
            .get()
            .expect("`Sudoku` must be set first");
        
        for i in 0..9 {
            for j in 0..9 {
                let cell = self.get_cell(i, j).get();
                let value = sudoku.get(i, j).get_val();
                if value == 0 {
                    cell.set_label(" ");
                } else {
                    cell.set_label(&value.to_string());
                }
            }
        }
    }
}
