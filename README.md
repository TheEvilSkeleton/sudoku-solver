# Sudoku Solver

### Sudoku Solver is a GTk4+libadwaita application that solves sudokus using the Wave Function Collapse Algorithm.

## Development
---
1. Install [Gnome Builder](https://flathub.org/apps/details/org.gnome.Builder).
2. Clone the repository from within the Builder.
3. Clone `https://gitlab.com/cyberphantom52/sudoku-solver.git`
4. Press "Run Project" at the top.
